<?php

namespace App\Http\Controllers;

use App\Post;
use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Validator;
use App\Events\NewArticlePosted;

class PostController extends Controller
{
    public function index()
    {
        return PostResource::collection(Post::with('user')->orderByDesc('id')->paginate(20));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string|max:10000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->messages()
            ], 500);
        }

        $user = JWTAuth::parseToken()->toUser();

        $post = Post::create([
            'user_id' => $user->id,
            'title' => $request->title,
            'description' => $request->description
        ]);

//        $notification = [
//            'message' => 'New article posted by '.$user->name
//        ];

        //broadcast(new NewArticlePosted($notification))->toOthers();

        return new PostResource($post);
    }

    public function show($id)
    {
        return new PostResource(Post::FindOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string|max:10000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->messages()
            ], 500);
        }

        $post = Post::findOrFail($id);
        $post->title = $request->title;
        $post->description = $request->description;
        $post->save();

        return new PostResource($post);
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return new PostResource($post);
    }
}
