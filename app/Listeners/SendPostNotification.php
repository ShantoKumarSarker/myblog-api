<?php

namespace App\Listeners;

use App\Events\NewArticlePosted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPostNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewArticlePosted  $event
     * @return void
     */
    public function handle(NewArticlePosted $event)
    {
        return $event;
    }
}
