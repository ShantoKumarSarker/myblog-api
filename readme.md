This project was bootstrapped with A PHP framework for web artisans [Laravel](https://github.com/socketio/socket.io).

## Available Scripts and steps to run this app

### `git clone https://ShantoKumarSarker@bitbucket.org/ShantoKumarSarker/myblog-api.git`

* `cd ` to project folder.
* Run ` composer install `
* Save as the `.env.example` to `.env` and set your database information 
* Run ` php artisan key:generate` to generate the app key
* Run ` php artisan jwt:secret` to generate the JWT key
* Run ` php artisan migrate ` 
* Run ` php artisan db:seed ` 
* Run ` php artisan serve ` 

Now in terminal you can see that this app is live at : http://localhost:8000

**Note: Please don't close this terminal and make sure this app is served at port 8000 !**

Through this avobe commands your api will be ready with 1 user:
email: sk.bd2007@gmail.com
password: 123456
and 30 dummy posts.